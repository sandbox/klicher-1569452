Entity server (entity_server)
=====================================
Store entities created within different Drupal instances of a same project to ensure that their IDs will be uniques and permit the download of the content (add/update).

This server module is actually developped to handle the "Term" entity type first cause the taxonomy could often be structurant for a project, Views can be build on certain terms, etc. Developpement for other types of entities will be possible.

The Drupal instance will send is content via the "Entity client" (entity_client) module.

-------------------------------------
Log
-------------------------------------
2012-04-20  7.x-1.0-dev     Kristoffer Laurin-Racicot
